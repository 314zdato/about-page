var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require("gulp-rename");
var minifycss = require('gulp-minify-css');
var jsmin = require('gulp-jsmin');
var $    = require('gulp-load-plugins')();

var sassPaths = [
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src'
];

gulp.task('scss', function() {
  gulp.src('./assets/scss/app.scss')
    .pipe($.sass({
      includePaths: sassPaths
    })
      .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
      .pipe(minifycss())
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('./build/css'));

    gulp.src('./assets/scss/other.scss')
        .pipe($.sass())
        .pipe(minifycss())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./build/css'));
});

gulp.task('scripts', function(){

  gulp.src([
        './bower_components/jquery/dist/jquery.js',
        './node_modules/vivus/dist/vivus.js',
        './node_modules/masonry-layout/dist/masonry.pkgd.js',
        './bower_components/foundation-sites/dist/foundation.js'
      ])
      .pipe(concat('app.js'))
      .pipe(jsmin())
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('./build/js'));

  gulp.src('./assets/scripts/*.js')
      .pipe(concat('custom.js'))
      .pipe(jsmin())
      .pipe(rename({suffix: '.min'}))
      .pipe(gulp.dest('./build/js'));

});

gulp.task('watch', function() {
  gulp.watch('./assets/scss/**/*.scss' , ['scss']);
  gulp.watch('./assets/scripts/*.js' , ['scripts']);
});

gulp.task('default', ['scss','scripts']);
