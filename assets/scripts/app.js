$(document).foundation();
    var options = {autoPlay: true};
    var slider = new Foundation.Orbit($(".orbit"), options);
    var options2 = {autoPlay: true, timerDelay: 4000, animInFromRight: 'fade-in', animInFromLeft: 'fade-in', animOutToLeft:'fade-out', animOutToRight:'fade-out'};
    var slider2 = new Foundation.Orbit($(".orbit-1"), options2);
    var options3 = {autoPlay: true, timerDelay: 5800, animInFromRight: 'fade-in', animInFromLeft: 'fade-in', animOutToLeft:'fade-out', animOutToRight:'fade-out'};
    var slider3 = new Foundation.Orbit($(".orbit-2"), options3);
    var options4 = {autoPlay: true, timerDelay: 7500, animInFromRight: 'fade-in', animInFromLeft: 'fade-in', animOutToLeft:'fade-out', animOutToRight:'fade-out'};
    var slider4 = new Foundation.Orbit($(".orbit-3"), options4);
    var options5 = {autoPlay: true, timerDelay: 6700, animInFromRight: 'fade-in', animInFromLeft: 'fade-in', animOutToLeft:'fade-out', animOutToRight:'fade-out'};
    var slider5 = new Foundation.Orbit($(".orbit-4"), options5);
    var options6 = {autoPlay: true, timerDelay: 7100, animInFromRight: 'fade-in', animInFromLeft: 'fade-in', animOutToLeft:'fade-out', animOutToRight:'fade-out'};
    var slider6 = new Foundation.Orbit($(".orbit-5"), options6);

var msnry = new Masonry( '.grid', {
    itemSelector: '.grid-item',
    columnWidth: '.grid-sizer',
    percentPosition: true
});
$(document).ready(function(){
    ymaps.ready(init);
    var myMap;
    function init(){
        myMap = new ymaps.Map ("map", {
            center: [57.118955271498635,65.56568799999994],
            zoom: 16,
            controls: ['zoomControl']
        });
        myMap.behaviors.disable('scrollZoom');
        // Создает стиль
        // Создание метки
        var myPlacemark = new ymaps.Placemark([57.1185467339013,65.56616006878657], {
            hintContent: 'Office Creative'
        },{
            // Опции.
            // Необходимо указать данный тип макета.
            iconLayout: 'default#image',
            // Своё изображение иконки метки.
            iconImageHref: '../build/img/icon_location.svg',
            // Размеры метки.
            iconImageSize: [46, 46],
            // Смещение левого верхнего угла иконки относительно
            // её "ножки" (точки привязки).
            iconImageOffset: [-28, -56]
        });
        // Добавление метки на карту
        myMap.geoObjects.add(myPlacemark);
    }

    var s = skrollr.init({
            render: function(data) {
                var scroll = data.curTop;
                console.log(scroll);
                var wheight = $(".head").height();
                var point = wheight - 70;
                if (scroll >= point) {
                    $("header").addClass("scroll");
                } else {
                    $("header").removeClass("scroll");
                }
            },
            skrollrBody: 'skrollrb',
            forceHeight: false
        });
    $("#onthemap").click( function(){
        $(".map").toggleClass("open");
    });
    $(".closemap").click( function(){
        $(".map").toggleClass("open");
    });

    $("#mobile-menu").click( function(){
        $("#mobile-menu").toggleClass("is-active");
        $("header").toggleClass("active");
        $("body").toggleClass("hidden");
    });
    new Vivus('bear-icon', {duration: 100});
    new Vivus('cost-icon', {duration: 100});
    new Vivus('cycle-icon', {duration: 100});
});
$(document).ready(function(){
    $('a[href^="#"], a[href^="."]').click( function(){ // если в href начинается с # или ., то ловим клик
        var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href
        if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
            $('html, body').animate({ scrollTop: $(scroll_el).offset().top - 65 }, 1500); // анимируем скроолинг к элементу scroll_el
        }
        return false; // выключаем стандартное действие
    });
});
